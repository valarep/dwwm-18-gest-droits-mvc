<?php
namespace Valarep;

// début de l'application web

// Chargement automatique des classes
require_once "vendor/autoload.php";

// initialisation des sessions
$default_values['connected'] = false;
$default_values['error'] = false;
$default_values['errorMessage'] = "";
$default_values['utilisateur'] = (object)[];
$default_values['roles'] = [];
$default_values['droits'] = [];
Session::init($default_values);

/*
Session::init([
    'connected' => false,
    'error' => false,
    'errorMessage' => "",
    'utilisateur' => (object)[],
    'roles' => [],
    'droits' => [],
]);*/
Session::start();

// Routage principal
$router = new Router();
$router->addRoute(new Route("/", "UtilisateurController"));
$router->addRoute(new Route("/user/{*}", "UtilisateurController"));
$router->addRoute(new Route("/users", "UtilisateurController"));

$route = $router->findRoute();

if ($route)
{
    $route->execute();
}
else
{
    // Erreur 404
    echo "Page not found";
}