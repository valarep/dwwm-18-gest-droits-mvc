            <nav class="navbar navbar-expand-md navbar-light bg-light">
            <a class="navbar-brand" href="#">Mon App</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarAdminContent" aria-controls="navbarContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarContent">
                <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="#">Menu 1</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Menu 2</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Menu 3</a>
                </li>
<?php require "dropdown_admin.html.php"; ?>
<?php require "dropdown_dir.html.php"; ?>
<?php require "dropdown_user.html.php"; ?>
                </ul>
            </div>
            </nav>