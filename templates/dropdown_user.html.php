<?php 
use Valarep\Session;
if(Session::has_role("Utilisateur(s)")): 
?>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdownUser" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Menu Utilisateur
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownUser">
                        <a class="dropdown-item" href="#">Action Utilisateur 1</a>
                        <a class="dropdown-item" href="#">Action Utilisateur 2</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Autre</a>
                    </div>
                </li>
<?php endif; ?>