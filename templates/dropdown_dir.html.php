<?php 
use Valarep\Session;
if(Session::has_role("Directeur(s)")): 
?>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdownDir" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Menu Directeur
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownDir">
                        <a class="dropdown-item" href="#">Action Directeur 1</a>
                        <a class="dropdown-item" href="#">Action Directeur 2</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Autre</a>
                    </div>
                </li>
<?php endif; ?>