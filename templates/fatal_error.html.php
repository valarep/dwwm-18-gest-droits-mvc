<!DOCTYPE html>
<html>
    <head>
    </head>
    <body>
        <p>Application actuellement indisponible.</p>
        <p>Veuillez contacter un administrateur.</p>
<?php if(isset($code)): ?>
        <p>(Code d'erreur <?= $code ?>)</p>
<?php endif; ?>
<?php if($debugMode): ?>
        <p>(Exception : <?= $ex->getMessage(); ?>)</p>
<?php endif; ?>
    </body>
</html>