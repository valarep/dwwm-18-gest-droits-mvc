<?php 
use Valarep\Session;
if(Session::has_role("Administrateur(s)")): 
?>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdownAdmin" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Menu Admin
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownAdmin">
<?php if(Session::has_droit("/users")): ?>
                        <a class="dropdown-item" href="#">Liste des utilisateurs</a>
<?php endif; ?>
<?php if(Session::has_droit("/user/create")): ?>
                        <a class="dropdown-item" href="#">Ajouter un utilisateur</a>
<?php endif; ?>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Autre</a>
                    </div>
                </li>
<?php endif; ?>