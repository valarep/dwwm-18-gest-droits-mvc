<?php 
use Valarep\Session;
?>
<!DOCTYPE html>
<html>
<?php require "head.html.php"; ?>
    <body>
<?php require "navbar.html.php"; ?>
        <div class="container">
            <h1>Home</h1>
<?php if($error): ?>
            <p><?= $errorMessage; ?></p>
<?php endif; ?>
<?php 
if($connected) {
    require "disconnection_form.html.php";
} else {
    require "connection_form.html.php";
}
?>
            <form method="post">
<?php if(Session::has_droit("/user/{id}")): ?>
            <button type="submit" formaction="<?= $path; ?>/user/15" class="btn btn-outline-secondary">Consulter</button>
<?php endif; ?>
<?php if(Session::has_droit("/user/update/{id}")): ?>
            <button type="submit" formaction="<?= $path; ?>/user/update/15" class="btn btn-outline-secondary">Modifier</button>
<?php endif; ?>
<?php if(Session::has_droit("/user/delete/{id}")): ?>
            <button type="submit" formaction="<?= $path; ?>/user/delete/15" class="btn btn-outline-secondary">Supprimer</button>
<?php endif; ?>
            </form>
<?php require "footer.html.php"; ?>
<?php require "scripts.html.php"; ?>
        </div>
    </body>
</html>