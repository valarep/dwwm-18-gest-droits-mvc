<?php
namespace Valarep\dao;

use \PDO;
use \Exception;

class UtilisateurDao
{
    /**
     * get Utilisateur from database
     * @param $login : Utilisateur login
     * @param $password : Utilisateur password
     * @return returns Utilisateur if login, password matches, or null 
     */
    public static function get($login, $password)
    {
        $dbh = Dao::open();

        $query = "SELECT * 
                    FROM `utilisateur` 
                    WHERE `login` = :login 
                    AND `password` = :password;";
        
        $sth = $dbh->prepare($query);

        $sth->bindParam(":login", $login);
        $sth->bindParam(":password", $password);

        $res = $sth->execute();
        if (! $res)
        {
            // debug
            // $error = $sth->errorInfo();
            // die($error[2]);
        }

        if ($sth->rowCount())
        {
            // connexion réussie
            // Utilisateur
            $sth->setFetchMode(
                PDO::FETCH_CLASS,
                "Valarep\\objects\\Utilisateur"
            );
            $item = $sth->fetch();
        }
        else
        {
            // connexion échouée
            // null
            $item = null;
        }
        Dao::close();

        return $item;
   } 
}