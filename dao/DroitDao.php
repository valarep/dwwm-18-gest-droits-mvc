<?php
namespace Valarep\dao;

use PDO;
use Exception;

class DroitDao
{
    /**
     * get Utilisateur from database
     * @param $id_utilisateur : id de l'utilisateur
     * @return array liste des droits de l'utilisateur
     */
    public static function getAll($id_utilisateur)
    {
        $dbh = Dao::open();

        $query = "SELECT DISTINCT `droit`.*
        FROM `droit`
        INNER JOIN `role_droit` 
          ON `role_droit`.`id_droit` = `droit`.`id`
        INNER JOIN `utilisateur_role` 
          ON `role_droit`.`id_role` = `utilisateur_role`.`id_role`
        WHERE `id_utilisateur` = :id_utilisateur;";
        
        $sth = $dbh->prepare($query);
        $sth->bindParam(":id_utilisateur", $id_utilisateur);

        $res = $sth->execute();
        if (! $res)
        {
            // debug
            // $error = $sth->errorInfo();
            // die($error[2]);
        }

        $sth->setFetchMode(
            PDO::FETCH_CLASS,
            "Valarep\\objects\\Droit"
        );

        $items = $sth->fetchAll();

        Dao::close();

        return $items;
   } 
}