<?php
namespace Valarep\dao;

use PDO;
use Exception;

class RoleDao
{
    /**
     * get Utilisateur from database
     * @param $id_utilisateur : id de l'utilisateur
     * @return array liste des roles de l'utilisateur
     */
    public static function getAll($id_utilisateur)
    {
        $dbh = Dao::open();

        $query = "SELECT `role`.*
        FROM `role`
        INNER JOIN `utilisateur_role` 
          ON `id_role` = `role`.`id`
        WHERE `id_utilisateur` = :id_utilisateur;";
        
        $sth = $dbh->prepare($query);
        $sth->bindParam(":id_utilisateur", $id_utilisateur);

        $res = $sth->execute();
        if (! $res)
        {
            // debug
            // $error = $sth->errorInfo();
            // die($error[2]);
        }

        $sth->setFetchMode(
            PDO::FETCH_CLASS,
            "Valarep\\objects\\Role"
        );

        $items = $sth->fetchAll();

        Dao::close();

        return $items;
   } 
}