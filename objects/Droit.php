<?php
namespace Valarep\objects;

use Valarep\dao\DroitDao;

class Droit
{
    public $id;
    public $nom;

    public static function getAll($id_utilisateur)
    {
        return DroitDao::getAll($id_utilisateur);
    }

    public function __toString()
    {
        return $this->nom;
    }
}