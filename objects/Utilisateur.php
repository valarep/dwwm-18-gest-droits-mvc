<?php
namespace Valarep\objects;

use Valarep\dao\UtilisateurDao;

class Utilisateur
{
    public $id;
    public $login;
    public $password;

    public static function get($login, $password)
    {
        return UtilisateurDao::get($login, $password);
    }
}