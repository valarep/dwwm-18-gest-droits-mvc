<?php
namespace Valarep\objects;

use Valarep\dao\RoleDao;

class Role
{
    public $id;
    public $nom;

    public static function getAll($id_utilisateur)
    {
        return RoleDao::getAll($id_utilisateur);
    }

    public function __toString()
    {
        return $this->nom;
    }
}