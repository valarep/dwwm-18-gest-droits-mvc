<?php
namespace Valarep;

use Exception;
use Valarep\objects\Utilisateur;
use Valarep\objects\Role;
use Valarep\objects\Droit;

class Session
{
    private static $default_values = [];

    public static function start()
    {
        session_start();
        //session_unset();
        self::reset();
        self::checkUserSession();
        //var_dump($_SESSION);
    }

    public static function init($default_values = [])
    {
        self::$default_values = $default_values;
    }

    private static function reset()
    {
        // valeur(s) par défaut de la session
        if (empty($_SESSION))
        {
            $_SESSION = self::$default_values;
        }
    }

    public static function unset()
    {
        session_unset();
        self::reset();
    }

    public static function get($name)
    {
        if (!isset($_SESSION[$name]))
        {
            throw new Exception("La variable de session $name n'est pas définie");
        }
        return $_SESSION[$name];
    }

    public static function set($name, $value)
    {
        $_SESSION[$name] = $value;
    }

    private static function checkUserSession()
    {
        if ($_SESSION['connected']){
            // si l'utilisateur a ouvert une session

            // Récupération des login et password de la session
            $login = $_SESSION['utilisateur']->login;
            $password = $_SESSION['utilisateur']->password;

            $utilisateur = Utilisateur::get($login, $password);

            if ($utilisateur == null)
            {
                // Session est invalide
                // Réinitialisation de la session
                self::unset();

                $_SESSION['error'] = true ;
                $_SESSION['errorMessage'] = "Session invalide." ;
            }
            else
            {
                // Mise à jour de la variable de Session
                $_SESSION['utilisateur'] = $utilisateur;
                $_SESSION['roles'] = Role::getAll($utilisateur->id);
                $_SESSION['droits'] = Droit::getAll($utilisateur->id);
            }
        }
    }

    /**
     * Retourne vrai si l'utilisteur a le role fournit en paramètre
     * @param $role le role cherché
     * @return bool vrai si l'utilisateur a le role
     */
    public static function has_role($role)
    {
        $roles = $_SESSION['roles'];
        $result = false;
        $count = count($roles);
        for($i = 0; $i < $count && ! $result; $i++)
        {
            $result = $role == $roles[$i]->nom;
        }
        return $result;
    }

    /**
     * Retourne vrai si l'utilisteur a le role fournit en paramètre
     * @param $role le role cherché
     * @return bool vrai si l'utilisateur a le role
     */
    public static function has_droit($droit)
    {
        $droits = $_SESSION['droits'];
        $result = false;
        $count = count($droits);
        for($i = 0; $i < $count && ! $result; $i++)
        {
            $result = $droit == $droits[$i]->nom;
        }
        return $result;
    }
}