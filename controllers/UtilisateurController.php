<?php
namespace Valarep\controllers;

use Valarep\Route;
use Valarep\Router;
use Valarep\Session;
use Valarep\View;
use Valarep\objects\Utilisateur;

class UtilisateurController
{
    public static function route()
    {
        // Routage secondaire
        $router = new Router();
        $router->addRoute(new Route("/", "UtilisateurController", "homeAction"));
        $router->addRoute(new Route("/user/connect", "UtilisateurController", "connectAction"));
        $router->addRoute(new Route("/user/disconnect", "UtilisateurController", "disconnectAction"));
        $router->addRoute(new Route("/users", "UtilisateurController", "listAction"));
        $router->addRoute(new Route("/user/update/{id}", "UtilisateurController", "updateAction"));
        $router->addRoute(new Route("/user/delete/{id}", "UtilisateurController", "deleteAction"));
        $router->addRoute(new Route("/user/{id}", "UtilisateurController", "editAction"));

        $route = $router->findRoute();

        if ($route)
        {
            $route->execute();
        }
        else
        {
            // Erreur 404
            echo "Page not found";
        }
    }

    public static function homeAction()
    {
        $connected = Session::get("connected");
        $error = Session::get("error");
        $errorMessage = Session::get("errorMessage");
        $utilisateur = Session::get("utilisateur");
        $roles = Session::get("roles");
        $droits = Session::get("droits");

        $title = "Mon App";

        //echo "homeAction works!";
        View::setTemplate("home");

        View::bindVariable("title", $title);
        View::bindVariable("connected", $connected);
        View::bindVariable("error", $error);
        View::bindVariable("errorMessage", $errorMessage);
        View::bindVariable("utilisateur", $utilisateur);
        View::bindVariable("roles", $roles);
        View::bindVariable("droits", $droits);

        View::display();
    }

    public static function connectAction()
    {
        //echo "connectAction works!";

        // récupération des données du formulaire
        $login = filter_input(INPUT_POST, "login", FILTER_SANITIZE_STRING);
        $password = filter_input(INPUT_POST, "password", FILTER_SANITIZE_STRING);

        $salt = $login;
        $salted_password = $password . $salt;
        $md5_password = md5($salted_password);
        $utilisateur = Utilisateur::get($login, $md5_password);

        if ($utilisateur)
        {
            // tout va bien
            // session valide
            Session::set('utilisateur', $utilisateur);
            Session::set('connected', true);
        }
        else
        {
            // erreur de login ou de password
            Session::set('error', true);      
            Session::set('errorMessage', "Login or password error !");      
        }

        $router = new Router();
        $path = $router->getBasePath();

        header("location: {$path}/");
    }

    public static function disconnectAction()
    {
        //echo "disconnectAction works!";
        Session::unset();

        $router = new Router();
        $path = $router->getBasePath();

        header("location: {$path}/");
    }

    public static function listAction() {}
    public static function editAction($id) 
    {
        if (! Session::has_droit("/user/{id}"))
        {
            // pas le droit !
            echo "Pas le droit d'éditer l'utilisateur.";
        }
        else
        {
            echo "ok.";
        }
    }
    public static function updateAction() 
    {
        if (! Session::has_droit("/user/update/{id}"))
        {
            // pas le droit !
            echo "Pas le droit de modifier l'utilisateur.";
        }
        else
        {
            echo "ok.";
        }
    }
    public static function deleteAction() 
    {
        if (! Session::has_droit("/user/delete/{id}"))
        {
            // pas le droit !
            echo "Pas le droit de supprimer l'utilisateur.";
        }
        else
        {
            echo "ok.";
        }
    }
}